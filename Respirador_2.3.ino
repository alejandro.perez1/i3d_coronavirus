/*
Respirador v2.1
WORK IN PROGRESS
*/







#include <AccelStepper.h>
#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD


const int stepsPerRevolution = 200;
const int microsteps = 32; 


#define dirPin 9
#define stepPin 10
#define motorInterfaceType 1

#define DT 3
#define CLK 2
#define SW 4

#define buzzerPin 12
#define valveEspPin 7
#define valveInsPin 7



AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4); 

int rpm = 15;
float recorrido = 0.50; 
float porcentajeInspiratorio = 0.6;
float porcentajePausa = 0.0;
float tIns;
float tPausa;
float tEsp;
int pos0=0; 

float velocidadUno = 0.0; 
float velocidadDos = 0.0; 


byte DialPos;
byte Last_DialPos;

void calcularConstantes()
{
  float tCiclo = 60 / rpm; //Tiempo de ciclo en segundos
  tIns = tCiclo * porcentajeInspiratorio;
  tPausa = tCiclo * porcentajePausa;
  tEsp = tCiclo - (tIns + tPausa);

  velocidadUno = (stepsPerRevolution*microsteps*recorrido / 2) / tIns;
  velocidadDos = (stepsPerRevolution*microsteps*recorrido / 2) / tEsp;
}

void calibrar()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar posicion ");
  lcd.setCursor(0,1);
  lcd.print("inicial ");
  lcd.setCursor(0,3);
  lcd.print("Click para empezar");
  stepper.setMaxSpeed(1000);
  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) pos0 = pos0+0.5*microsteps;
    if (DialPos == 3 && Last_DialPos == 2)pos0 = pos0-0.5*microsteps;
    else {
        stepper.moveTo(pos0);
        stepper.runToPosition();
      }
    Last_DialPos = DialPos;
    
  }
  while (!digitalRead(SW)){}
  
}

void settings()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar RPMs    ");
  lcd.print(rpm);
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) rpm++;
    if (DialPos == 3 && Last_DialPos == 2) rpm--;
    else {
        lcd.setCursor(16,0);
        if(rpm>=0 && rpm<10)lcd.print(" ");
        lcd.print(rpm);
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}


  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar porcentaje");
  lcd.setCursor(0,1);
  lcd.print("inspirado     ");
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajeInspiratorio = porcentajeInspiratorio+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajeInspiratorio = porcentajeInspiratorio-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajeInspiratorio*100)>=0 && (int)(porcentajeInspiratorio*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajeInspiratorio*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}

  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar porcentaje");
  lcd.setCursor(0,1);
  lcd.print("pausado        ");
  lcd.print((int)(porcentajePausa*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajePausa = porcentajePausa+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajePausa = porcentajePausa-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajePausa*100)>=0 && (int)(porcentajePausa*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajePausa*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}


  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar recorrido    ");
  lcd.setCursor(14,1);
  lcd.print((int)(recorrido*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) recorrido = recorrido+0.05;
    if (DialPos == 3 && Last_DialPos == 2) recorrido = recorrido-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(recorrido*100)>=0 && (int)(recorrido*100)<10)lcd.print(" ");
        lcd.print((int)(recorrido*100)); 
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}


}


void setup() {
  Serial.begin(9600);
  stepper.setAcceleration(10000);
  lcd.init();
  lcd.backlight();
  
  pinMode(DT, INPUT);   
  pinMode(CLK, INPUT); 
  pinMode(SW, INPUT); 
  digitalWrite(SW, HIGH);

  pinMode (valveEspPin, OUTPUT);
  pinMode (valveInsPin, OUTPUT);
  
  pinMode (buzzerPin, OUTPUT);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Bienvenido");
  delay (2000);

  
  settings();
  calibrar();
  calcularConstantes();  
  
  lcd.clear();
  lcd.setCursor(13,0);
  lcd.print(rpm);
  lcd.print(" RPM");
  lcd.setCursor(1,2);
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("% Ins    ");
  lcd.print((int)(recorrido*100));
  lcd.print("% Vol");
}

void loop() { 


    lcd.setCursor(0, 0);
    lcd.print("Espiracion");

    digitalWrite(valveEspPin, LOW);
//    digitalWrite(valveInsPin, HIGH);
    stepper.setMaxSpeed(velocidadDos);
    stepper.moveTo(pos0 - (stepsPerRevolution*microsteps*recorrido/2));
    stepper.runToPosition();
    delay(10);


    lcd.setCursor(0, 0);
    lcd.print("Inspiracion"); 
    
    digitalWrite(valveEspPin, HIGH);
//    digitalWrite(valveInsPin, LOW);
    stepper.setMaxSpeed(velocidadUno);
    stepper.moveTo(pos0);
    stepper.runToPosition();
    delay(10);

    lcd.setCursor(0, 0);
    lcd.print("Pausa       ");
    delay(tPausa*1000);
   
   
}